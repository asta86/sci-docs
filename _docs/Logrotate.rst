*********
Logrotate
*********
.. highlight:: bash

Check conf file for syntax errors.
==================================
::

    /usr/sbin/logrotate -d /usr/local/etc/logrotate.d/wwwlogs

Clear log file after it grows over a certain size.
==================================================
:doc:`Apache` example conf.

Rotate count.
==================================
Determines how many archived logs are returned before logrotate starts
deleting the older ones. For example: rotate 4 tells logrotate to keep four
archived logs at a time. If four archived logs exist when the log is rotated
again, the oldest one is deleted.

Test current config results without applying them.
==================================================
::

    /usr/sbin/logrotate -d /usr/local/etc/logrotate.d/confFile


