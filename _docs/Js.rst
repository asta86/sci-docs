**********
JavaScript
**********
.. highlight:: javascript

Install ESLint and airbnb rules.
================================
.. highlight:: bash

::

    cd projectDir
    npm install --save-dev babel-eslint
    npx install-peerdeps --dev eslint-config-airbnb # for react projects
    npx install-peerdeps --dev eslint-config-airbnb-base # for projects that don't use react

* npm 5+ required.
* Packages are installed for the current project and saved as devDependencies in package.json.
* `Reference. <https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb>`_

.. highlight:: javascript

Create file .eslintrc with the following content. ::

    {
      "env": {
        "browser": true,
        "es6": true,
        "node": true
      },
      "extends": "airbnb",
      "parser": "babel-eslint",
      "rules": {
        "class-methods-use-this": "off",
        "import/extensions": "off",
        "jsx-a11y/anchor-is-valid": ["error", { "components": ["Link"], "specialLink": ["to"] }],
        "jsx-a11y/label-has-for": "off",
        "react/destructuring-assignment": "off",
        "react/prefer-stateless-function": "off",
        "react/prop-types": "off",
        "react/jsx-one-expression-per-line": "off"
      }
    }
    
* Enable or disable individual rules in the rules object.

JQuery select specifying attributes of node and ancestors.
==========================================================

::

    $("#grandfatherId .parentClass #targetNodeId")


