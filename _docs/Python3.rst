***************************
Python 3 notes and snippets
***************************
.. highlight:: python3

Check if object is of valid type.
=================================
::

    hasattr(object, var_or_method_name)

* Arguments are an object and a string.
* Recommended way compatible with duck typing.

.. highlight:: bash

Conda.
======
Clean cached files (may fix http errors).
-----------------------------------------
::

    conda clean --all

Create virtual env with certain packages passed as last arguments.
------------------------------------------------------------------
* Add python=3.x after venvName to specify interpreter version.

::

    conda create -n venvName pip

Remove entire virtual env.
--------------------------
::

    conda remove -n venvName --all

Display current settings.
-------------------------
::

    conda config --show

Add a value to a setting key.
-----------------------------
::

    conda config --append envs_dirs /path

Remove a value from a setting key.
----------------------------------
::

    conda config --remove envs_dirs /path

Update anaconda.
----------------
::

    conda update conda
    conda update anaconda

Update all packages in a virtual env.
-------------------------------------
::

    conda update --all --prefix /path/to/prefix

.. highlight:: python3

Define abstract class and method.
=================================
::

    from abc import ABC, abstractmethod

    class AbstractClassName(ABC):

        @abstractmethod
        def abstract_method_name(self):
            pass

Do not execute code inside if block when module is imported.
============================================================
::

    	if __name__ == '__main__':

Encrypt strings with cryptography.
==================================
* Requires pypi package cryptography.

::

    from cryptography.fernet import Fernet

    key = Fernet.generate_key()
    crypt = Fernet(key)
    token = crypt.encrypt(b"my deep dark secret")

    # write encrypted text to file
    with open('tst', 'w') as fh:
        fh.write(str(token, 'utf-8'))


    # read encrypted text from file (use the same key as before)
    key = b'ixH1fRLHpq6Ci7m_MPMII2NzToy7ipb0ebuhEpNI8j8='
    crypt = Fernet(key)

    with open('tst', 'r') as fh:
	    crypt.decrypt(fh.read().encode())

Flask.
======
Routes.
-------
Route generic url and read url parameter into var.
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
::

	@app.route("/<string:name>")
	def hello(name):
		return f"Hello, {name}"

Route specific url.
^^^^^^^^^^^^^^^^^^^
::

	from flask import Flask

	app = Flask(__name__)

	@app.route("/")
	def index():
		return "Hello, world!"

Sessions.
---------
Define var specific to each session. ::

	session["var_name"] = value

Use independent sessions in app. ::

	from flask import session
	from flask_session import Session
	Session(app)

Set app file (shell command).
-----------------------------
::

	export FLASK_APP=fileName.py

Start devel webserver (shell command).
--------------------------------------
::

	flask run

.. highlight:: jinja

Templates (jinja2).
-------------------
If statement.
^^^^^^^^^^^^^
::

	{% if bool_var %}
		true branch
	{% else %}
		false branch
	{% endif %}

Inheritance.
^^^^^^^^^^^^
In layout/superclass file. ::

	{% block_name %}{% endblock %}

In subclass file. ::

	{% extends "layout_file.html" %}
	{% block_name %} specific content {% endblock %}

Link to url tied to specified function.
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
::

	<a href="{{ url_for('func_name') }}">Link text...</a>

Loop.
^^^^^
::

	<ul>
		{% for name in names %}
			<li>{{ name }}<li>
		{% endfor %}
	</ul>

.. highlight:: python3

Pass argument to template.
^^^^^^^^^^^^^^^^^^^^^^^^^^
::

	from flask import Flask, render_template

	app = Flask(__name__)

	@app.route("/")
	def index():
		headline = "Hello, world!"
		return render_template("index.html", headline=headline)

inside html template value can be accessed using {{ headline }}.

.. highlight:: jinja

Pass form data to function hello.
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
::

    <form action="{{ url_for('hello') }}" method="post">
        <input type="text" name="name" placeholder="Enter Your Name">
        <button>Submit</button>
    </form>

.. highlight:: python3

Inside py file. ::

    from flask import request
	@app.route("/hello", methods=["POST"])
	def hello():
		name = request.form.get("name")
		return render_template("hello.html", name=name)

Looping methods.
================
Loop over a single list with a regular for-in.
----------------------------------------------
::

	for n in numbers:
            print(n)

Loop over a list while keeping track of indexes with enumerate.
---------------------------------------------------------------
::

	for index, name in enumerate(pokemons, start=1):
		print("Pokemon {}: {}".format(index, name))

* Using 1 as first index, 0 by default if not passed.

Loop over multiple lists at the same time with zip.
---------------------------------------------------
::

	for header, rows in zip(headers, columns):
		print("{}: {}".format(header, ", ".join(rows)))

Math.
=====
Division.
---------
Floor div (without reminder).
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
::

    //

Common division (always returns a float value).
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
::

    /

Multiple assignment.
====================
Unpack in a for loop.
---------------------
::

	for key, value in person_dictionary.items():
		print(f"Key {key} has value {value}")

Assign first character to x, second to y.
-----------------------------------------
::

    x, y = 'hi'

Assign first number to x, second to y.
--------------------------------------
::

    x, y = [10, 20]

Capture remaining items after unpacking. The two lines are equivalent.
----------------------------------------------------------------------
::

        head, middle, last = numbers[0], numbers[1:-1], numbers[-1]
	head, *middle, last = numbers

.. highlight:: none

Nose.
=====
* Requires pypi package nose.

Run tests with nose.
--------------------
::

    nosetests

Using setup file.
-----------------
::

    python3 setup.py test

Pip.
====
Install any needed packages specified in requirements.txt.
----------------------------------------------------------
::

	pip install -r requirements.txt

Install local package that uses setuptools.
-------------------------------------------
::

	pip install .

Install package with a symlink.
-------------------------------
::

	pip install -e .

* Changes to the source files will be immediately available.

.. highlight:: python3

Pipenv.
=======
Create new venv associated with current dir using specified python version.
---------------------------------------------------------------------------
::

    pipenv --python 3.7

Install package and add it to Pipfile.
--------------------------------------
::

    pipenv install [OPTIONS] [PACKAGES]

* If no package names are given attempts to read packages to install from Pipfile.

Remove project virtualenv (inferred from current directory).
------------------------------------------------------------
::

    pipenv --rm

Run a shell within the projects' venv.
--------------------------------------
::

    pipenv shell

Run command using python from projects' venv.
---------------------------------------------
::

    pipenv run command args

* Environment variables can be passed, i. e. PYTHONPATH=../ pipenv run command.

Uninstall package from current projects' venv.
----------------------------------------------
::

    pipenv uninstall [OPTIONS] [PACKAGES]


Property notation.
==================
::

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, val):
        self.__name = val

Psycopg2.
=========
Asynchronous notifications.
---------------------------
::

	import select
	import psycopg2
	import psycopg2.extensions

	conn = psycopg2.connect(DSN)
	conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)

	curs = conn.cursor()
	curs.execute("LISTEN test;")

	print "Waiting for notifications on channel 'test'"
	while 1:
		if select.select([conn],[],[],5) == ([],[],[]):
		    print "Timeout"
		else:
		    conn.poll()
		    while conn.notifies:
		        notify = conn.notifies.pop(0)
		        print "Got NOTIFY:", notify.pid, notify.channel, notify.payload

.. highlight:: none

Running the script and executing a command such as NOTIFY test, 'hello' in a
separate psql shell, the output may look similar to: ::

	Waiting for notifications on channel 'test'
	Timeout
	Timeout
	Got NOTIFY: 6535 test hello
	Timeout

* `Reference. <http://initd.org/psycopg/docs/advanced.html#asynchronous-notifications>`_

PyQt.
=====
* Fedora packages: qt5-designer python-qt5.

After saving ui file with qt designer, convert it to python.
------------------------------------------------------------
::

	pyuic5 gui.ui > gui.py

Then it can be imported from the python project.

.. highlight:: python3

Read command line input into var.
=================================
::

    path = input("Enter path and press enter: ")

Read environment variable (i. e. db credentials).
=================================================
::

    import os
    env_var = os.getenv("VARNAME")

Recursion.
==========
Recursively revert string.
--------------------------
::

	def rev(str):
		if len(str) == 0:
			return str
		else:
			# last char + output of rev for the rest of str
                        # starting from the end
			return str[-1] + rev(str[:-1])

Slice notation.
===============
::

        sequence = [1, 2, 3]
        # First element.
        sequence[0]

        # Last element.
        sequence[-1]

        # Second last element.
        sequence[-2]

        # Tail of the sequence (starting after first or nth elem).
        sequence[1:]

        # Tail of the sequence (starting after last or nth elem).
        sequence[:-1]

SQLAlchemy.
===========
Connect to database using independent sessions.
-----------------------------------------------
::

	from sqlalchemy import create_engine
	from sqlalchemy.orm import scoped_session, sessionmaker

	engine = create_engine("DATABASE_URL")
	db = scoped_session(sessionmaker(bind=engine))

String formatting.
==================
::

	name = "tst"
        # Format string with format method.
	print("hello, {}".format(name))

        # Format string with prefix f (3.6+).
	print(f"hello, {name}")

        # Multi-line literal (newline can be escaped with \).
        """first line
        second"""

.. highlight:: none

Unittest.
=========
Run test module in terminal.
----------------------------
::

    python -m unittest -v test_module

Use discovery to find and run tests in dir.
-------------------------------------------
::

    python -m unittest discover -v '/dir'

* If dir name is not quoted, it will not work if it contains spaces.

