***
Git
***
.. highlight:: bash

Branches.
=========
Create branch.
--------------
::

    git branch branchName

List branches.
--------------
::

    git branch

Merge target branch with current.
---------------------------------
::

    git merge targetBranchName

Move to branch.
---------------
::

    git checkout branchName

Push local branch not existent on remote repo.
----------------------------------------------
::

    git push --set-upstream origin localBranchName

Change remote's URL from HTTPS to SSH.
======================================
::

    git remote set-url origin git@github.com:USERNAME/REPOSITORY.git

Create release.
===============
::

    git tag tagName
    git push -u origin tagName

* Appears as tagName in the project's releases page on GitHub.

Download upstream and automatically merge local with it (fast forward).
=======================================================================
::

    git pull

List commits in current branch.
===============================
::

    git log

Remove and stop tracking file.
==============================
::

    git rm fileName
    git commit -m "remove file"
    echo "fileName" >> .gitignore
    git commit -m "stop tracking file"

Undo changes.
=============
Return to state of specific commit.
-----------------------------------
::

    git reset --hard commitHash

Revert changes to a file if they haven’t been committed yet.
------------------------------------------------------------
::

    git checkout -- file

Revert everything from HEAD back to commit without losing history.
-------------------------------------------------------------------------------
::

    git revert --no-commit 0766c053..HEAD
    git commit

